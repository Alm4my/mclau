<?php
    class Router{
        public static function classCall(){
            // HELPERS
            require_once '../helper/ErrorMessage.php';
            require_once '../helper/StickyInput.php';
            
            
            // MODELS
            require_once '../model/DatabaseConnection.php';
            
            
            // CONTROLLERS
            require_once '../controller/Constants.php';
            require_once '../controller/FormValidator.php';
            require_once '../controller/FormSanitizer.php';
            
            
            // VIEWS
            require_once '../view/Affiche.php';
            require_once '../view/Header.php';
            
        }
    }