<?php
    
    class DatabaseConnection{
        /* Properties */
        private PDO $db;
        
        /* Methods */
        public function __construct(){
            $information = parse_ini_file('db');
            $this -> db = $this -> establishConnection($information);
        }
        
        /**
         * @param $information array of dsn, database user and password.
         * @return PDO connection to the database
         */
        private function establishConnection(Array $information){
            try {
                $link = new PDO($information['dsn'], $information['dbuser'], $information['dbpass']);
                $link -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                return $link;
            }
            catch (PDOException $exception){
                exit("Couldn't connect to the database: \n" . $exception->getMessage());
            }
        }
        
        /**
         * @return PDO connection
         */
        public function getDb(): PDO {
            return $this -> db;
        }
    }

