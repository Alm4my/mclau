<?php
    
    
    class Header {
        public static function callHeader($title){
            echo <<<__HTML
            <!DOCTYPE html>
            <html lang="en">
            <head>
                <meta charset="UTF-8">
                <title>$title</title>
                <meta charset="utf-8">
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
                <meta name="description" content="">
                    <!-- Bootstrap core CSS -->
                <link href="assets/style/bootstrap.min.css" rel="stylesheet">
                <!-- Custom styles for this template -->
                <link href="assets/style/signin.css" rel="stylesheet">
            <!--                <link href="assets/style/style.css" rel="stylesheet">-->
            </head>
            <body>
                <!--                <div class="wrapper">-->
            __HTML;
        }
    }