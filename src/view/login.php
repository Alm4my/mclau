<?php
    // Router
    require_once "../helper/Router.php";
    Router ::classCall();
    
    // Create socket for validation
    $socket = new FormValidator();
    
    // Process
    if ($_SERVER["REQUEST_METHOD"] == 'POST'){
        $username = FormSanitizer::sanitizeFormUsername($_POST['username']);
        $password = FormSanitizer::sanitizeFormPassword($_POST['password']);
    
        $success = $socket -> validateLogin($username, $password);
        
        if ($success) {
            // Store Session
            $_SESSION["userLoggedIn"] = $username;
            header("Location: index.php");
        }
    }
    
    // Header
    $header = new Header();
    $header -> callHeader("Inscription");
    
    // Output
    $affiche = new Affiche();
    $affiche -> login($socket);