<?php
    
    
    class Affiche {
        public static function demande(){
            echo <<<__HTML
                
                <div class="formulaire">
                    <div class='title'>
                        <h1>Demande d'Intervention</h1>
                    </div>
                    <form method="post">
                        <label>
                            Date de demande :
                            <input type="date" id="dateDemande" name="date">
                        </label> <br>
                        <label>
                            Heure de demande :
                            <input type="text" name="heure">
                        </label> <br>
                        <label>
                            Problème :
                            <textarea name="probleme" id="probleme" rows="5" cols="50" placeholder="i.e. Mon ecran ne s'allume pas" >
                            </textarea>
                        </label> <br>
                        <label>
                            Le problème porte sur :
                            <select class="probleme" name="problemePorte">
                                <option>Ecran</option>
                                <option>Unité Centrale</option>
                                <option>Imprimante</option>
                            </select>
                        </label> <br>
                        <label>
                            Agent :
                            <select class="agents" name="agents">
                                <option> Fofana Lamine </option>
                            </select>
                            <a href="agent.php"> S'inscrire</a>
                        </label> <br>
                        <label>
                            Numéro de téléphone :
                            <input type="number" name="telephone">
                        </label> <br>
                        <label>
                            Fonction :
                            <select class="fonction" name="fonction">
                                <option>Secretaire</option>
                                <option>Agent</option>
                            </select>
                            <a href="fonction.php"> Ouvrir Ecran Fonction</a>
                        </label> <br>
                        <label>
                            Statut :
                            <select class="statut" name="statut">
                                <option>Fonctionnaire</option>
                                <option>Contractuel</option>
                            </select>
                            <a href="statut.php"> Ouvrir Ecran Statut</a>
                        </label> <br>
                        <label>
                            Structure :
                            <select class="structure" name="structure">
                                <option>Direction du Domaine Urbain</option>
                                <option>Test</option>
                            </select>
                            <a href="structure.php"> Ouvrir Ecran Structure</a>
                        </label> <br>
                        <label>
                            Local :
                            <select class="local" name="local">
                                <option>Tour D</option>
                                <option>Tour C</option>
                            </select>
                            <a href="local.php"> Ouvrir Ecran Local</a>
                        </label> <br>
                        <label>
                            Bureau :
                            <select class="bureau" name="bureau">
                                <option>TA_2E_P10</option>
                                <option>TA_2E_P20</option>
                            </select>
                            <a href="bureau.php"> Ouvrir Ecran Bureau</a>
                        </label> <br>
                        <div class="submitBTNS">
                            <input type="submit" name="envoyer" value="Enregistrer">
                            <input type="button" name="annuler" value="Annuler">
                            <input type="button" name="supprimer" value="Supprimer">
                            <input type="button" name="fermer" value="Fermer">
                        </div>
                    </form>
                </div>
            </body>
            </html>
__HTML;

        }
        
        // First Design
        public function inscriptions($socket) {
            print
                <<<_HTML_
                            <div class="signInCont">
                                <div class="column">
                                    <div class="header">
                                        <a href="index.php">
                                            <img src="assets/img/1.jpeg" alt="[Pelicula Logo]" style="width: 120px; ">
                                        </a>
                                        <h3> S'inscrire</h3>
                                        <span>pour utiliser ce service</span>
                                    </div>
                                    <form method="post" action="register.php">
    _HTML_;
    
            // first name error
            echo $socket->getError(Constants::F_NAME_ERR);
    
            // HTML
            printf( '<label>
                        <input type="text" placeholder="Prénom" name="firstName" value="%s" required>
                    </label>', StickyInput::value('firstName'));
    
            // last name error
            echo $socket->getError(Constants::L_NAME_ERR);
    
            // HTML
            printf (' <label>
                        <input type="text" placeholder="Nom" name="lastName" value="%s" required>
                    </label>', StickyInput::value('lastName'));
    
            // username errors
            echo $socket->getError(Constants::USERNAME_LENGTH_ERR);
            echo $socket->getError(Constants::USERNAME_TAKEN_ERR);
    
            // HTML
            printf ('<label>
                        <input type="text" placeholder="Nom d\'utilisateur" name="username"  value="%s" required>
                    </label>', StickyInput::value('username'));
    
            // email errors
            echo $socket->getError(Constants::EMAIL_MATCH_ERR);
            echo $socket->getError(Constants::EMAIL_VAL_ERR);
            echo $socket->getError(Constants::EMAIL_TAKEN_ERR);
    
            // HTML
            printf ('<label>
                        <input type="email" placeholder="Email" name="email" value="%s" required>
                    </label>
                    <label>
                        <input type="email" placeholder="Confirm Email" name="email2" value="%s" required>
                    </label>', StickyInput::value('email'), StickyInput::value('email2'));
    
            // password errors
            echo $socket->getError(Constants::PASSWORD_LENGTH_ERR);
            echo $socket->getError(Constants::PASSWORD_MATCH_ERR);
    
            // HTML
            print '<label>
                        <input type="password" placeholder="Mot de Passe" name="password" required>
                    </label>
                    <label>
                        <input type="password" placeholder="Confirmez votre mot de passe" name="password2" required>
                    </label>';
    
            // phone number errors
            echo $socket->getError(Constants::PHONE_NUMBER_TYPE_ERR);
            echo $socket->getError(Constants::PHONE_NUMBER_LENGTH_ERR);
    
            // HTML
            printf('<label>
                       <input type="text" placeholder="Numéro de téléphone" name="phone" value="%s" required>
                    </label>', StickyInput::value('phone'));
    
            // Fonctions
            echo $socket -> getError(Constants::GEN_ERR);
            echo $socket -> getError(Constants::INVALID_FONCTION);
            print("<label> Fonction :
                       <select name='fonction'>");
            
            foreach (Constants::FONCTIONS as $val)
                      print "<option> $val </option>" ;
            echo "</select> </label>";
            
            // Statut Travailleur
            echo $socket -> getError(Constants::GEN_ERR);
            echo $socket -> getError(Constants::INVALID_FONCTION);
            print("<label> Statut :
                       <select name='statutTravailleur'>");
    
            foreach (Constants::STATUT_TRAVAILLEUR as $val)
                print "<option> $val </option>" ;
            echo "</select> </label>";
            
            // Type structure
            echo $socket -> getError(Constants::GEN_ERR);
            echo $socket -> getError(Constants::INVALID_FONCTION);
            print("<label> Type de Structure :
                       <select name='typeDeLastructure'>");
    
            foreach (Constants::TYPE_DE_STRUCTURES as $val)
                print "<option> $val </option>" ;
            echo "</select> </label>";
            
            // Nom Structure
            echo $socket -> getError(Constants::GEN_ERR);
            echo $socket -> getError(Constants::INVALID_FONCTION);
            print("<label> Nom de la Structure :
                       <select name='fonction'>");
    
            foreach (Constants::LISTE_DES_STRUCTURES as $val)
                print "<option> $val </option>" ;
            echo "</select> </label>";
            
            // Ville
            echo $socket -> getError(Constants::GEN_ERR);
            echo $socket -> getError(Constants::INVALID_FONCTION);
            printf ('<label>
                        <input type="text" placeholder="Ville" name="ville"  value="%s" required>
                    </label>', StickyInput::value('ville'));
            
            // Commune
            echo $socket -> getError(Constants::GEN_ERR);
            echo $socket -> getError(Constants::INVALID_FONCTION);
            printf ('<label>
                        <input type="text" placeholder="Commune" name="commune"  value="%s" required>
                    </label>', StickyInput::value('commune'));
            
            // Local
            echo $socket -> getError(Constants::GEN_ERR);
            echo $socket -> getError(Constants::INVALID_FONCTION);
            printf ('<label>
                        <input type="text" placeholder="Local" name="local"  value="%s" required>
                    </label>', StickyInput::value('local'));
            
            // Etage
            echo $socket -> getError(Constants::GEN_ERR);
            echo $socket -> getError(Constants::INVALID_FONCTION);
            printf ('<label>
                        <input type="text" placeholder="Etage" name="etage"  value="%s" required>
                    </label>', StickyInput::value('etage'));
            
            // bureau
            echo $socket -> getError(Constants::GEN_ERR);
            echo $socket -> getError(Constants::INVALID_FONCTION);
            printf ('<label>
                        <input type="text" placeholder="Bureau" name="bureau"  value="%s" required>
                    </label>', StickyInput::value('bureau'));
            
            // HTML
            print <<<_HTML_
                            <input type="submit" value="Register" name="submitRegForm">
                        
                        </form>
                        
                        <a href="login.php" class="SignInMessage">Already have an account? Sign in here!</a>
                    
                    </div>
                </div>
            </body>
        </html>
_HTML_;
        }
        
        public function login($socket){
            echo <<<_HTML_
              <body class="text-center">
                <form class="form-signin" method="post" >
              <img class="mb-4" src="assets/img/1.jpeg" alt="" width="172" height="172">
              <h1 class="h3 mb-3 font-weight-normal">Se Connecter</h1>
            _HTML_;
            
            echo $socket -> getError(Constants::LOGIN_FAILED);
            
            echo <<<_HTML_
              <label for="inputEmail" class="sr-only">nom d'utilisateur :</label>
              <input type="text" name="username" class="form-control" placeholder="Nom d'utilisateur" required autofocus>
              <label for="inputPassword" class="sr-only">mot de passe :</label>
              <input type="password" name="password" class="form-control" placeholder="Mot de passe" required>
              <div class="checkbox mb-3" >
<!--                <label>-->
<!--                  <input type="checkbox" value="remember-me"> Remember me-->
<!--                </label>-->
              </div>
  <button class="btn btn-lg btn-primary btn-block" type="submit">Connexion</button>
  
  <br><p>Pas encore inscrit? <a href="inscription.php">Cliquez-ici!</a></p>
  <p class="mt-5 mb-3 text-muted">&copy;  2020</p>
</form>
</body>
</html>

_HTML_;
        }
        public function inscription($socket){
            echo <<<_HTML_
      <body class="text-center">
        <form class="form-signin scroll" method="post">
        
              <img class="mb-4" src="assets/img/1.jpeg" alt="" width="172" height="172">
              <h1 class="h3 mb-3 font-weight-normal">Inscription </h1>
                <p>Déjà inscrit? <a href="login.php">Connectez-vous!</a></p>
     _HTML_;
          // last name error
            echo $socket->getError(Constants::L_NAME_ERR);
            printf('<label for="inputNom" class="sr-only">Nom :</label>
              <input type="text" name="nom" id="inputNom" value="%s" class="form-control"
              placeholder="Nom" required autofocus>', StickyInput::value("nom"));
              
          // first name error
           echo $socket->getError(Constants::F_NAME_ERR);
           printf('<label for="inputPrenoms" class="sr-only">Prénoms :</label>
              <input type="text" name="prenoms" value="%s" id="inputPrenoms" class="form-control"
              placeholder="Prénoms" required >', StickyInput::value("prenoms"));
           
           // username errors
            echo $socket->getError(Constants::USERNAME_LENGTH_ERR);
            echo $socket->getError(Constants::USERNAME_TAKEN_ERR);
            printf('<label for="inputNomUtilisateur" class="sr-only">Nom d\'utilisateur :</label>
              <input type="text" name="nomUtilisateur" id="inputNomUtilisateur" value="%s" class="form-control"
               placeholder="Nom d\'utilisateur" required >', StickyInput::value("nomUtilisateur"));
              
            // email errors
            echo $socket->getError(Constants::EMAIL_VAL_ERR);
            echo $socket->getError(Constants::EMAIL_TAKEN_ERR);
            printf('<label for="inputEmail" class="sr-only">Addresse Email</label>
              <input type="email" id="inputEmail" class="form-control" value="%s" name="email"
              placeholder="Email address" required >', StickyInput::value("email"));
              
           // password errors
            echo $socket->getError(Constants::PASSWORD_LENGTH_ERR);
            echo $socket->getError(Constants::PASSWORD_MATCH_ERR);
            printf('  <label for="inputMotDePasse" class="sr-only">Mot de Passe :</label>
              <input type="password" name="motDePasse" id="inputMotDePasse" class="form-control" value="%s"
              placeholder="Mot de passe" required >
              
              <label for="inputMotDePasse2" class="sr-only">Mot de Passe2 :</label>
              <input type="password" name="motDePasse2" id="inputMotDePasse2" class="form-control" value="%s"
              placeholder="Re-entrez votre mot de passe" required >'
                , StickyInput::value("motDePasse"), StickyInput::value("motDePasse2"));
              
             // phone number errors
            echo $socket->getError(Constants::PHONE_NUMBER_TYPE_ERR);
            echo $socket->getError(Constants::PHONE_NUMBER_LENGTH_ERR);
            printf('<label for="inputNumero" class="sr-only">Numéro de téléphone :</label>
              <input type="tel" name="numero" class="form-control" value="%s"
              placeholder="Numéro de téléphone :" required >', StickyInput::value("numero"));
             
             // Ville
//            echo $socket -> getError(Constants::GEN_ERR);
//            echo $socket -> getError(Constants::INVALID_VALUE_ERR);
            printf('  <label for="inputVille" class="sr-only">Ville :</label>
              <input type="text" name="ville" id="inputVille" class="form-control" value="%s"
              placeholder="Ville" required >', StickyInput::value("ville"));
              
             // Commune
//            echo $socket -> getError(Constants::GEN_ERR);
//            echo $socket -> getError(Constants::INVALID_VALUE_ERR);
            printf('  <label for="inputCommune" class="sr-only">Commune :</label>
              <input type="text" name="commune" id="inputCommune" class="form-control" value="%s"
              placeholder="Commune" required >', StickyInput::value("commune"));
              
              // Local
//            echo $socket -> getError(Constants::GEN_ERR);
//            echo $socket -> getError(Constants::INVALID_VALUE_ERR);
            printf('  <label for="inputLocal" class="sr-only">Local :</label>
              <input type="text" name="local" id="inputLocal" class="form-control" value="%s"
              placeholder="Local" required >', StickyInput::value("local"));
              
              // Etage
//            echo $socket -> getError(Constants::GEN_ERR);
//            echo $socket -> getError(Constants::INVALID_VALUE_ERR);
            printf(' <label for="inputEtage" class="sr-only">Etage :</label>
              <input type="text" name="etage" id="inputEtage" class="form-control" value="%s"
               placeholder="Etage" required >', StickyInput::value("etage"));
              
              // bureau
//            echo $socket -> getError(Constants::GEN_ERR);
//            echo $socket -> getError(Constants::INVALID_VALUE_ERR);
            printf('  <label for="inputBureau" class="sr-only">Bureau :</label>
              <input type="text" name="bureau" id="inputBureau" class="form-control" value="%s"
               placeholder="Bureau" required >', StickyInput::value("bureau"));
           
            print ' <div class="form-row">
                     <div class="col ">';
              // Fonctions
                echo $socket -> getError(Constants::INVALID_FONCTION);
                print("<label>
                           <select name='fonction' class='form-control' id='noLeftMargin'  >
                           <option disabled selected>Fonction</option>");

                foreach (Constants::FONCTIONS as $val)
                          echo "<option> $val </option>" ;
                echo "</select> </label> </div>";
                
             // Statut Travailleur
                echo $socket -> getError(Constants::INVALID_STATUT);
                print("<div class='col'><label>
                           <select name='statutTravailleur' class='form-control'>
                           <option disabled selected>Statut </option>");
        
                foreach (Constants::STATUT_TRAVAILLEUR as $val)
                    echo "<option> $val </option>" ;
                echo "</select> </label></div></div>";
    
            print ' <div class="form-row form-group">
                     <div class="col ">';
            
             // Type structure
                echo $socket -> getError(Constants::INVALID_TYPE_STRUCTURE);
                print("<div class='col'><label>
                           <select name='typeDeLaStructure' class='form-control'>
                           <option disabled selected>Type de votre structure</option>");
        
                foreach (Constants::TYPE_DE_STRUCTURES as $val)
                    echo "<option> $val </option>" ;
                echo "</select> </label></div>";
            
            // Nom Structure
                echo $socket -> getError(Constants::INVALID_NOM_STRUCTURE);
                print("<div class='col'><label>
                           <select name='nomDeStructure' class='form-control'>
                           <option disabled selected>Nom de votre Structure</option>");
        
                foreach (Constants::LISTE_DES_STRUCTURES as $val)
                    echo "<option> $val </option>" ;
                echo "</select> </label> </div>";
               
           echo <<<_HTML_
              </div> </div>
              <button class="btn btn-lg btn-primary btn-block" type="submit">S'inscrire</button>
              <p class="mt-5 mb-3 text-muted">&copy; 2020</p>
        </form>
    </body>
</html>

_HTML_;

        }
    }