<!DOCTYPE html>
<html lang="en">
    <head>
        <title>
            Register
        </title>
        <link rel="shortcut icon" type="image/png" href="assets/img/p.svg">
        <link rel="stylesheet" type="text/css" href="assets/style/style.css">
    </head>
    <body>
        <div class="signInCont">
            <div class="column">
                <div class="header">
                    <img src="assets/img/p5.svg" alt="[Pelicula Logo]">
                    <h3> Sign Up</h3>
                    <span>to use our service</span>
                </div>
                <form method="post" action="register.php">
                    <?php echo $socket->getError(Constants::F_NAME_ERR); ?>
                    <label>
                        <input type="text" placeholder="First Name" name="firstName" required>
                    </label>
                    <?php echo $socket->getError(Constants::L_NAME_ERR); ?>
                    <label>
                        <input type="text" placeholder="Last Name" name="lastName" required>
                    </label>
                    <?php echo $socket->getError(Constants::USERNAME_TAKEN_ERR); ?>
                    <label>
                        <input type="text" placeholder="Username" name="username" required>
                    </label>
                    <?php echo $socket->getError(Constants::EMAIL_MATCH_ERR); ?>
                    <?php echo $socket->getError(Constants::EMAIL_VAL_ERR); ?>
                    <label>
                        <input type="email" placeholder="Email" name="email" required>
                    </label>
                    <label>
                        <input type="email" placeholder="Confirm Email" name="email2" required>
                    </label>
                    <?php echo $socket->getError(Constants::PASSWORD_LENGTH_ERR); ?>
                    <?php echo $socket->getError(Constants::PASSWORD_MATCH_ERR); ?>
                    <label>
                        <input type="password" placeholder="Password" name="password" required>
                    </label>
                    <label>
                        <input type="password" placeholder="Confirm Password" name="password2" required>
                    </label>
                    <?php echo $socket->getError(Constants::PHONE_NUMBER_TYPE_ERR); ?>
                    <?php echo $socket->getError(Constants::PHONE_NUMBER_LENGTH_ERR); ?>
                    <label>
                        <input type="text" placeholder="Phone Number" name="phone" required>
                    </label>
                    
                    <input type="submit" value="Register" name="submitRegForm">
                
                </form>
                
                <a href="login.php" class="SignInMessage">Already have an account? Sign in here!</a>
            
            </div>
        </div>
    </body>
</html>