<?php
    
    // Router
    require_once "../helper/Router.php";
    Router ::classCall();
    
    // Create socket for validation
    $socket = new FormValidator();
    
    // Process
    if ($_SERVER["REQUEST_METHOD"] == 'POST'){
        // Sanitize input
        $firstName = FormSanitizer::sanitizeFormStringDefault($_POST['prenoms']);
        $lastName = FormSanitizer::sanitizeFormStringDefault($_POST['nom']);
        $username = FormSanitizer::sanitizeFormUsername($_POST['nomUtilisateur']);
        $email = FormSanitizer::sanitizeFormEmail($_POST['email']);
        $password = FormSanitizer::sanitizeFormPassword($_POST['motDePasse']);
        $password2 = FormSanitizer::sanitizeFormPassword($_POST['motDePasse2']);
        $phone = FormSanitizer::sanitizePhoneNumber($_POST['numero']);
        $fonction = FormSanitizer::sanitizeFormStringDefault($_POST['fonction']);
        $statutTravailleur = FormSanitizer::sanitizeFormStringDefault($_POST['statutTravailleur']);
        $typeDeStructure = FormSanitizer::sanitizeFormStringDefault($_POST['typeDeLaStructure']);
        $nomDeStructure = FormSanitizer::sanitizeFormStringDefault($_POST['nomDeStructure']);
        $ville = FormSanitizer::sanitizeFormStringDefault($_POST['ville']);
        $commune = FormSanitizer::sanitizeFormStringDefault($_POST['commune']);
        $local = FormSanitizer::sanitizeFormStringDefault($_POST['local']);
        $etage = FormSanitizer::sanitizeFormStringDefault($_POST['etage']);
        $bureau = FormSanitizer::sanitizeFormStringDefault($_POST['bureau']);
        
        // Valider et Inscrire
        $success = $socket -> validateRegister($firstName, $lastName, $username, $email, $password, $password2, $phone,
                                   $fonction, $statutTravailleur, $typeDeStructure, $nomDeStructure, $ville, $commune,
                                   $local, $etage, $bureau);
        // load index.php if registration was successful
        if ($success) {
            // Store Session
            $_SESSION["userLoggedIn"] = $username;
            header("Location: demande.php");
        }
    }
    
    // Header
    $header = new Header();
    $header -> callHeader("Inscription");
    
    // Output
    $affiche = new Affiche();
    $affiche -> inscription($socket);