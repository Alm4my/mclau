<?php
    /*
     * * @Author: Alm4my
     */
    require_once '../model/DatabaseConnection.php';
    require_once 'Compte.php';
    require_once 'Constants.php';
    
    class FormValidator {
        // Property: Array that contains the errors
        private array $errors = array();
    
        /**
         * Validate inputs according to methods specifications
         *
         * @param $fn string prenoms
         * @param $ln string nom
         * @param $un string nom d'utilisateur
         * @param $em1 string email
         * @param $em2 string repetition email
         * @param $pass1 string mot de passe
         * @param $pass2 string mot de passe repetition
         * @param $phone string numero de telephone
         *
         * @param $fonction string fonction de la personne e.g. Ministre
         * @param $statutTravailleur string statut du travailleur e.g. fonctionnaire
         * @param $typeDeStructure string type de la structure e.g. externe
         * @param $nomDeStructure string nom de la structure e.g. SNDI
         * @param $ville string
         * @param $commune string
         * @param $local string
         * @param $etage string
         * @param $bureau string
         *
         * @return boolean
         */
        public function validateRegister($fn, $ln, $un, $em1, $pass1, $pass2, $phone,
                                         $fonction, $statutTravailleur, $typeDeStructure,
                                         $nomDeStructure, $ville, $commune, $local, $etage, $bureau){
            $this->validateFirstName($fn);
            $this->validateLastName($ln);
            $this->validateUserName($un);
            $this->validateEmail($em1);
            $this->validatePasswords($pass1, $pass2);
            $this->validatePhone($phone);
            $this->validateFonction($fonction);
            $this->validateStatutTravailleur($statutTravailleur);
            $this->typeDeStructure($typeDeStructure);
            $this->nomDeStructure($nomDeStructure);
//            $this->ville($ville);
//            $this->commune($commune);
//            $this->local($local);
//            $this->etage($etage);
//            $this->bureau($bureau);
            
            // account socket
            $account = new Compte();
            
            // if no error, register. else return false.
            if (empty($this->errors))
                return $account -> register($fn, $ln, $un, $em1, $pass1, $phone, $fonction, $statutTravailleur, $typeDeStructure,
                                            $nomDeStructure, $ville, $commune, $local, $etage, $bureau);
            
            return false;
        }
    
        /**
         * Login the user and add error if unsuccessful
         *
         * @param string $username username of the user
         * @param string $password password of the user
         *
         * @return bool true if login successful false otherwise
         */
        public function validateLogin(string $username, string $password) {
            // create account socket
            $account = new Compte();
            
            // run login query
           $query = $account -> login($username, $password);
            
            // return true if the query was passed successfully
            if ($query)
                return true;
           
            // push error if unsuccessful
            array_push($this -> errors, Constants::LOGIN_FAILED);
            return false;
        }
        
        
        /*
         * Validate FirstName
         */
        private function validateFirstName($firstName){
            $firstName = trim($firstName) ?? '';
            $this->length($firstName, 2, 100, Constants::F_NAME_ERR);
        }
    
        /*
         * Validate LastName
         */
        private function validateLastName($lastName){
            $lastName = trim($lastName) ?? '';
            $this->length($lastName, 2, 100, Constants::L_NAME_ERR);
        }
    
        /*
         * Validate Username
         */
        private function validateUserName($username){
            // check username length
            $username = trim($username) ?? '';
            $len = $this->length($username, 3, 100, Constants::USERNAME_LENGTH_ERR);
           
            // if username length is incorrect, stop.
            if (! $len)
                return;
    
            // check if username is registered
            $db = new DatabaseConnection();
            $query = $db -> getDb() -> prepare("
                                            SELECT * FROM UTILISATEUR
                                            WHERE NOM_UTILISATEUR=:username");
            $query -> bindValue(":username", $username);
            $query -> execute();
            if ($query->rowCount() != 0)
                array_push($this->errors, Constants::USERNAME_TAKEN_ERR);
        }
    
        /*
         * Validate Email
         */
        private function validateEmail($email1){
            // trim emails or replace them by empty if null
            $email1 = trim($email1) ?? '';
            
            // validate email with filters since they are already the same
            if (! filter_var($email1, FILTER_VALIDATE_EMAIL)){
                array_push($this->errors, Constants::EMAIL_VAL_ERR);
                return; // stop if email not valid
            }
            
            // check if email is already registered
            $db = new DatabaseConnection();
            $query = $db -> getDb() -> prepare("SELECT * FROM UTILISATEUR
                                                         WHERE EMAIL=:email");
            $query -> bindValue(":email", $email1);
            $query -> execute();
            
            if ($query -> rowCount() != 0)
                array_push($this->errors, Constants::EMAIL_TAKEN_ERR);
        }
    
        /*
         * Validate Passwords
         */
        private function validatePasswords($password1, $password2){
            // check if password's length is correct
            $len = $this->length($password1, 5, 255, Constants::PASSWORD_LENGTH_ERR);
            if (! $len)
                return;
            
            // check if passwords match
            $this->match($password1, $password2, Constants::PASSWORD_MATCH_ERR);
        }
    
        /*
         * Validate Phone Number
         */
        private function validatePhone($phoneNumber){
            $phoneNumber = trim($phoneNumber);
            if (! preg_match("/^[0-9]{8}/", $phoneNumber)){
               array_push($this->errors, Constants::PHONE_NUMBER_TYPE_ERR);
               return;
           }
    
            if (strlen($phoneNumber) !== Constants::PHONE_NUMBER_LENGTH) {
               array_push($this -> errors, Constants::PHONE_NUMBER_LENGTH_ERR);
           }
        }
    
        /**
         *  Add predefined error in the error array if the inputs do not match
         *
         * @param $input1
         * @param $input2
         * @param $errorConstant
         *
         * @return bool according to how input match
         */
        private function match($input1, $input2, $errorConstant){
            if ($input1 != $input2) {
                array_push($this -> errors, $errorConstant);
                return false;
            }
            return true;
        }
        
        /**
         * Check length of input and add error message accordingly
         * @return bool according to the length
         */
        private function length($input, $minLength, $maxLength, $errorConstant){
            if ( (strlen($input) < $minLength) || (strlen($input) > $maxLength) ) {
                array_push($this -> errors, $errorConstant);
                return false;
            }
            return true;
        }
    
        /**
         * Checks if an error exists and displays it
         * @param $error string the error to be searched in the array.
         *
         * @return string the error if found or nothing.
         */
        public function getError($error){
            if (in_array($error, $this->errors))
                return "<span class='errorMsg'>" . $error . "</span>" ;
            return '';
        }
    
        /**
         * @return array of errors for the instance.
         */
        public function getErrors(): array {
            return $this -> errors;
        }
        
        private function validateString($input, $listOfValues, $errorConstant){
            $input = trim($input) ?? '';
//            $this -> length($input, 1, 100,Constants::GEN_ERR);
            
            if (! in_array($input, $listOfValues))
                array_push($this->errors, $errorConstant);
        }
    
        private function validateFonction(string $fonction) {
            $this->validateString($fonction, Constants::FONCTIONS, Constants::INVALID_FONCTION);
        }
    
        private function validateStatutTravailleur(string $statutTravailleur) {
            $this->validateString($statutTravailleur, Constants::STATUT_TRAVAILLEUR,Constants::INVALID_STATUT);
        }
    
        private function typeDeStructure(string $typeDeStructure) {
            $this->validateString($typeDeStructure, Constants::TYPE_DE_STRUCTURES,Constants::INVALID_TYPE_STRUCTURE);
        }
    
        private function nomDeStructure(string $nomDeStructure) {
            $this->validateString($nomDeStructure, Constants::LISTE_DES_STRUCTURES, Constants::INVALID_NOM_STRUCTURE);
        }
        
        // j'ai laisse ca vide au cas ou nous
        // devrions ajouter de nouvelles villes et ajouter des checks
        private function ville(string $ville) {
        }
    
        private function commune(string $commune) {
        }
    
        private function local(string $local) {
        }
    
        private function etage(string $etage) {
        }
    
        private function bureau(string $bureau) {
        }
    
    }