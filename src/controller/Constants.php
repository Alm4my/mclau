<?php


class Constants
{
  const F_NAME_ERR = "Votre prénom doit être entre 2 et 100 charactères.";
  const L_NAME_ERR = "Votre nom doit être entre 2 et 100 charactères.";
  const USERNAME_TAKEN_ERR = "Vous ne pouvez pas utiliser ce nom d'utilisateur. Veuillez en choisir un autre.";
  const USERNAME_LENGTH_ERR = "Votre nom d'utilisateur doit être entre 3 et 100 charactères.";
  const EMAIL_MATCH_ERR = "Vos addresses emails ne sont pas pareilles. Veuillez les verifier.";
  const EMAIL_VAL_ERR = "Votre addresse email n'est pas correcte. Veuillez verifier.";
  const EMAIL_TAKEN_ERR = "Cette address email est déjà en cours d'utilisation. <a href='recovery.php'> Avez-vous oublié votre mot de passe? </a> ";
  const PASSWORD_MATCH_ERR = "Vos mots de passes ne sont pas pareilles. Veuillez les verifier.";
  const PASSWORD_LENGTH_ERR = "Votre mot de passe doit être superieur à 3 charactères.";
  const PHONE_NUMBER_LENGTH_ERR = "La longueur de votre numéro de téléphone est invalide. Il doit être composé de 8 chiffres.";
  const PHONE_NUMBER_TYPE_ERR = "Votre numéro de téléphone est invalide. Il doit être composé de 8 chiffres. i.e. 08080808.";
  const LOGIN_FAILED = "Le nom d'utilisateur ou le mot de passe est incorrect. <a href='recovery.php'> Avez-vous oublié vos identifiants? </a>";
  const GEN_ERR = "Ce champ doit être entre 1 et 100 charactères.";
  const INVALID_FONCTION = "La fonction entree n'est pas acceptable veuillez en selectionner une autre <br>";
  const INVALID_STATUT = "Le statut entré n'est pas acceptable veuillez en selectionner un autre <br>";
  const INVALID_TYPE_STRUCTURE = "Le type de structure entré n'est pas acceptable veuillez en selectionner un autre <br>";
  const INVALID_NOM_STRUCTURE = "Le nom de structure entré n'est pas acceptable veuillez en selectionner un autre <br>";
  const PHONE_NUMBER_LENGTH = 8;
  
  // array des informations
    const FONCTIONS = array('Ministre', 'Directeur', 'Agent');
    const STATUT_TRAVAILLEUR = array('Fonctionnaire', 'Contractuel', 'Stagiaire');
    const TYPE_DE_STRUCTURES = ['Externe', 'Interne'];
    const LISTE_DES_STRUCTURES = ['Cabinet', 'SNDI', 'MTN', 'ICMB', 'DDU', 'DAAF', 'DRH', 'SI'];
}