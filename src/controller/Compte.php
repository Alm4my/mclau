<?php
require_once '../model/DatabaseConnection.php';

class Compte{
    
    /**
     * Register Method
     *
     * @param $firstName string
     * @param $lastName string
     * @param $username string
     * @param $email string
     * @param $password string
     * @param $phone string
     *
     * @return bool true if registration was successful false otherwise.
     */
    public function register(string $firstName, string $lastName, string $username, string $email, string $password, string $phone,
                             string $fonction, string $statutTravailleur, string $typeDeStructure, string $nomDeStructure,
                             string $ville, string $commune, string $local, string $etage, string $bureau){
       return $this->insertUserInfo($firstName, $lastName, $username, $email, $password, $phone, $fonction, $statutTravailleur, $typeDeStructure, $nomDeStructure, $ville, $commune, $local, $etage, $bureau);
    }
    private function insertUserInfo($firstName, $lastName, $username, $email, $password, $phone,
                                    $fonction, $statutTravailleur, $typeDeStructure, $nomDeStructure, $ville, $commune, $local, $etage, $bureau){
        // hash the password with sha3-512 algorithm
        $password = hash("sha3-512", $password);
        
        // Create new database connection
        $db = new DatabaseConnection();
        
        // prepare the query
        $query = $db ->
                    getDb() ->
                        prepare(
                            "INSERT INTO UTILISATEUR(PRENOMS, NOM, NOM_UTILISATEUR, EMAIL, MOT_DE_PASSE, NUMERO_DE_TELEPHONE)
                                          VALUES (:fn, :ln, :un, :em, :pw, :phone )");
        
        // bind the parameters
        $query -> bindValue(":fn", $firstName);
        $query -> bindValue(":ln", $lastName);
        $query -> bindValue(":un", $username);
        $query -> bindValue(":em", $email);
        $query -> bindValue(":pw", $password);
        $query -> bindValue(":phone", $phone);
        
        $query2 = $db -> getDb() -> prepare("INSERT INTO TRAVAIL(FONCTION, STATUT_TRAVAILLEUR, TYPE_DE_STRUCTURE, NOM_DE_STRUCTURE, VILLE, COMMUNE, LOCAL, ETAGE, BUREAU, ID_UTILISATEUR)
                                            VALUES (:fonc, :statut, :typeStruct, :nomStruct, :ville, :commune, :loc, :etage, :bureau, :id)");
        
        $query2 -> bindValue(":fonc", $fonction);
        $query2 -> bindValue(":statut", $statutTravailleur);
        $query2 -> bindValue(":typeStruct", $typeDeStructure);
        $query2 -> bindValue(":nomStruct", $nomDeStructure);
        $query2 -> bindValue(":ville", $ville);
        $query2 -> bindValue(":commune", $commune);
        $query2 -> bindValue(":loc", $local);
        $query2 -> bindValue(":etage", $etage);
        $query2 -> bindValue(":bureau", $bureau);
    
        // execute second query if first is successful
        if ($query->execute()) {
            // Get ID of current user
            $query = $db -> getDb() -> prepare("SELECT ID FROM UTILISATEUR WHERE NOM_UTILISATEUR=:username AND EMAIL=:email");
            $query -> bindValue(":username", $username);
            $query -> bindValue(":email", $email);
            if ($query -> execute()) {
                $query2 -> bindValue(":id", $query->fetch()['ID']); //TODO CHECK
                return $query2 -> execute();
            }
        }
        return false;
    }
    
    /**
     * Login Method
     *
     * @param $username string username
     * @param $password string password
     *
     * @return bool true if login successful false otherwise
     */
    public function login($username, $password) {
        return $this->loginHelper($username, $password);
    }
    private function loginHelper($username, $password){
        // create database connection
        $db = new DatabaseConnection();
        
        // escaping special characters from username
        $username = strtr($username, array('_' => '\_', '%' => '\%'));
        
        // hashing password
        $password = hash("sha3-512", $password);
        
        // preparing query
        $query = $db ->
                    getDb() ->
                            prepare("SELECT * FROM UTILISATEUR WHERE NOM_UTILISATEUR=:un AND MOT_DE_PASSE=:pw ");
        
        // binding values
        $query -> bindValue(":un", $username);
        $query -> bindValue(":pw", $password);
        
        // executing queries
        $query -> execute();
        
        // return true if match or false otherwise
        if ($query -> rowCount() == 1) {
//            $query = $db -> getDb() -> query("SELECT ID FROM USER WHERE USERNAME=:un");
//            $uid = $query -> fetch();
            // Update last login time
            $row = $query -> fetch(PDO::FETCH_ASSOC);
            $query = $db -> getDb() -> prepare("INSERT INTO HISTORIQUE(ID_UTILISATEUR) VALUES (:uid)");
            $query -> bindValue(":uid", $row['ID']);
            $query -> execute();
            
            return true;
        }
        return false;
    }
    
}