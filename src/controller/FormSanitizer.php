<?php


class FormSanitizer
{
    // Sanitizing Static Methods
    public static function sanitizeFormStringDefault($input){
        $input = trim($input);
        filter_var($input,FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_HIGH);
        $input = str_replace(" ", "-", $input);
        $input = strtolower($input);
        $input = ucfirst($input);
        return $input;
    }
    
    public static function sanitizeFormStringDefaultWithNoCap($input) {
        $input = trim($input);
        filter_var($input, FILTER_SANITIZE_STRING, FILTER_FLAG_ENCODE_HIGH);
        $input = str_replace(" ", "-", $input);
        return $input;
    }
    
        public static function sanitizeFormUsername($input){
        $input = trim($input);
        $input = strip_tags($input);
        $input = str_replace(" ", "", $input);
        return $input;
    }

    public static function sanitizeFormPassword($input){
        // For security reasons we remove the tags.
        $input = strip_tags($input);
        return $input;
    }

    public static function sanitizeFormEmail($input){
        $input = trim($input);
        filter_var($input, FILTER_SANITIZE_EMAIL);
        $input = strip_tags($input);
        $input = str_replace(" ", "", $input);
        return $input;
    }

    public static function sanitizePhoneNumber($input){
        $input = trim($input);
        $input = strip_tags($input);
        $input = str_replace(" ", "", $input);
        return $input;
    }
}